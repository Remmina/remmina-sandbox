# Introduction
Sometimes, users may wait for connection or doing a time consuming process on remote desktop. It were great if Remmina had a mini-game.

Related issue: [#2899](https://gitlab.com/Remmina/Remmina/-/issues/2899)

# Notes
- The user that is going to need a break, will be more likely connected to a remote server, so the Remmina Toolbar would be the nearest and more logical place where to place such a button that runs the game.
- The button should simply Connect a Remmina Connection Profile (a .remmina file) that is linked to the game.
- It can't be embed as a window inside the Remmina Connection Window.
- Pay attention that GTK4 has more features about OpenGL.
