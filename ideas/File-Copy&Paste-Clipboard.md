# Introduction
Using RDP as remote desktop besides local desktop, it's often necessary to copy content and files between the hosts. This could make remote desktop usage so much more efficient than usage today without.

Related issue: [#2086](https://gitlab.com/Remmina/Remmina/-/issues/2086)
