# Introduction
Implement favorite connections to quickly access to the most used connections instead of doing search everytime.

Related issue: [#476](https://gitlab.com/Remmina/Remmina/-/issues/476)

# Notes
- This feature must be added to the GTK4 version of Remmina.(see [here](https://gitlab.com/Remmina/Remmina/-/merge_requests/2465))
