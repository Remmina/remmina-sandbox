# Introduction

Let's put in place a CI to automatize whatever we do by hand.

So far we do (almost) manually

1. Update po files (witha a script and manual commit)
2. Update developer documentation (with a script on @antenore server)
3. Release (quite manual), on Remmina (tag), launchpad, snap and flatpak.

# Developer documentation and translations

* Create a new project RemminaAuto
* Create a .gitlab-ci.yml that
  * Checkout the Remmina source code and Remmina gitlab pages code.
  * run the scripts/update-translations.sh
  * Commit the new translations  
  * Build the Doxygen doc
  * Commit new files in gitlab pages

## Notes

- The ci should be triggered only when merge requests are accepted and merged
- We should also check if we can submit issues through the CI, if yes, we can submit an issue to the translator when there is job for him.

