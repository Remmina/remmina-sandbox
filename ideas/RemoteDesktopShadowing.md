# Introduction
Give the possibility to share the same user session in remote desktop . You can use Remote Desktop Shadowing to remotely connect to user sessions on Windows computers. This feature is essentially an analog of Remote Assistance and allows administrators to remotely view and interact with the user’s desktop both on desktop versions (Windows 11 or 10) and on Windows Server RDS servers.

Related issue: [#2774](https://gitlab.com/Remmina/Remmina/-/issues/2774)

# Notes
- Remote Desktop Shadowing is not supported in FreeRDP at the moment.