# Introduction
If the connection fails, for example, the host has not booted yet, the window just shows "Close" button.
It would be nice to have a "Retry" button next to "Close".

Related issue: [#3005](https://gitlab.com/Remmina/Remmina/-/issues/3005) and [#2387](https://gitlab.com/Remmina/Remmina/-/issues/2387)
